﻿// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

using System;
using System.Collections.Generic;
using System.Linq;

namespace LinkedIn.NET.Members
{
    internal class LinkedInMember : ILinkedInMember
    {
        internal LinkedInMember()
        {
            Location = new LinkedInLocation();
            RelationToViewer = new LinkedInRelationToViewer();
        }

        #region BasicProfile members

        private readonly List<LinkedInPosition> _Positions = new List<LinkedInPosition>();

        public string Id { get; internal set; }

        public string FirstName { get; internal set; }

        public string LastName { get; internal set; }

        public string MaidenName { get; internal set; }

        public string FormattedName { get; internal set; }

        public string PhoneticFirstName { get; internal set; }

        public string PhoneticLastName { get; internal set; }

        public string FormattedPhoneticName { get; internal set; }

        public string Headline { get; internal set; }

        public LinkedInLocation Location { get; private set; }

        public string Industry { get; internal set; }

        public int ? Distance { get; internal set; }

        public LinkedInRelationToViewer RelationToViewer { get; private set; }

        public LinkedInShare CurrentShare { get; internal set; }

        public int ? NumConnections { get; internal set; }

        public bool ? NumConnectionsCapped { get; internal set; }

        public string Summary { get; internal set; }

        public string Specialities { get; internal set; }
        /// <summary>
        /// Gets a URL to the profile picture, if the member has associated one with their profile and it is visible to the requestor
        /// </summary>
        public string PictureUrl { get; internal set; }

        public string SiteStandardProfileRequest { get; internal set; }

        public LinkedInApiStandardProfileRequest ApiStandardProfileRquest { get; internal set; }

        public string PublicProfileUrl { get; internal set; }

        public IEnumerable<LinkedInPosition> Positions
        {
            get { return _Positions.AsEnumerable(); }
        }

        #endregion

        #region EmailProfile members

        public string EmailAddress { get; internal set; }

        #endregion

        #region FullProfile members

        private readonly List<LinkedInPublication> _Publications = new List<LinkedInPublication>();
        private readonly List<LinkedInPatent> _Patents = new List<LinkedInPatent>();
        private readonly List<LinkedInLanguage> _Languages = new List<LinkedInLanguage>();
        private readonly List<LinkedInSkill> _Skills = new List<LinkedInSkill>();
        private readonly List<LinkedInEducation> _Educations = new List<LinkedInEducation>();
        private readonly List<LinkedInCertification> _Certifications = new List<LinkedInCertification>();
        private readonly List<LinkedInCourse> _Courses = new List<LinkedInCourse>();
        private readonly List<LinkedInVolunteerExperience> _Volunteer = new List<LinkedInVolunteerExperience>();
        private readonly List<LinkedInPosition> _ThreeCurrentPositions = new List<LinkedInPosition>();
        private readonly List<LinkedInPosition> _ThreePastPositions = new List<LinkedInPosition>();
        private readonly List<LinkedInPersonBase> _RelatedProfileViews = new List<LinkedInPersonBase>();
        private readonly List<LinkedInMemberUrl> _MemberUrls = new List<LinkedInMemberUrl>();
        private readonly List<LinkedInRecommendation> _RecommendationsReceived = new List<LinkedInRecommendation>();
        private readonly List<LinkedInJobBookmark> _JobBookmarks = new List<LinkedInJobBookmark>();
        private readonly List<LinkedInPersonBase> _Connections = new List<LinkedInPersonBase>();

        public DateTime ? LastModifiedTimestamp { get; internal set; }

        public string ProposalComments { get; internal set; }

        public string Associations { get; internal set; }

        public string Interests { get; internal set; }

        public IEnumerable<LinkedInPublication> Publications
        {
            get { return _Publications.AsEnumerable(); }
        }

        public IEnumerable<LinkedInPatent> Patents
        {
            get { return _Patents.AsEnumerable(); }
        }

        public IEnumerable<LinkedInLanguage> Languages
        {
            get { return _Languages.AsEnumerable(); }
        }

        public IEnumerable<LinkedInSkill> Skills
        {
            get { return _Skills.AsEnumerable(); }
        }

        public IEnumerable<LinkedInEducation> Educations
        {
            get { return _Educations.AsEnumerable(); }
        }

        public IEnumerable<LinkedInCertification> Certifications
        {
            get { return _Certifications.AsEnumerable(); }
        }

        public IEnumerable<LinkedInCourse> Courses
        {
            get { return _Courses.AsEnumerable(); }
        }

        public IEnumerable<LinkedInVolunteerExperience> Volunteer
        {
            get { return _Volunteer.AsEnumerable(); }
        }

        public IEnumerable<LinkedInPosition> ThreeCurrentPositions
        {
            get { return _ThreeCurrentPositions.AsEnumerable(); }
        }

        public IEnumerable<LinkedInPosition> ThreePastPositions
        {
            get { return _ThreePastPositions.AsEnumerable(); }
        }

        public IEnumerable<LinkedInPersonBase> RelatedProfileViews
        {
            get { return _RelatedProfileViews.AsEnumerable(); }
        }

        public IEnumerable<LinkedInPersonBase> Connections
        {
            get { return _Connections.AsEnumerable(); }
        }

        public IEnumerable<LinkedInMemberUrl> MemberUrls
        {
            get { return _MemberUrls.AsEnumerable(); }
        }

        public LinkedInDate DateOfBirth { get; internal set; }

        public int NumRecommenders { get; internal set; }

        public IEnumerable<LinkedInRecommendation> RecommendationsReceived
        {
            get { return _RecommendationsReceived.AsEnumerable(); }
        }

        public string MfeedRssUrl { get; internal set; }

        public IEnumerable<LinkedInJobBookmark> JobBookmarks
        {
            get { return _JobBookmarks.AsEnumerable(); }
        }

        public LinkedInFollowing Following { get; internal set; }

        public LinkedInSuggestions Suggestions { get; internal set; }

        #endregion

        #region Internal procedures
        internal void AddPositions(IEnumerable<LinkedInPosition> positions)
        {
            _Positions.AddRange(positions);
        }
        internal void AddPublications(IEnumerable<LinkedInPublication> publications)
        {
            _Publications.AddRange(publications);
        }

        internal void AddPatents(IEnumerable<LinkedInPatent> patents)
        {
            _Patents.AddRange(patents);
        }

        internal void AddLanguages(IEnumerable<LinkedInLanguage> languages)
        {
            _Languages.AddRange(languages);
        }

        internal void AddSkills(IEnumerable<LinkedInSkill> skills)
        {
            _Skills.AddRange(skills);
        }

        internal void AddEducations(IEnumerable<LinkedInEducation> educations)
        {
            _Educations.AddRange(educations);
        }

        internal void AddCertifications(IEnumerable<LinkedInCertification> certifications)
        {
            _Certifications.AddRange(certifications);
        }

        internal void AddCourses(IEnumerable<LinkedInCourse> courses)
        {
            _Courses.AddRange(courses);
        }

        internal void AddVolunteer(IEnumerable<LinkedInVolunteerExperience> voluteer)
        {
            _Volunteer.AddRange(voluteer);
        }

        internal void AddPositions(IEnumerable<LinkedInPosition> positions, bool current)
        {
            if (current)
                _ThreeCurrentPositions.AddRange(positions);
            else
                _ThreePastPositions.AddRange(positions);
        }

        internal void AddRelatedProfileViews(IEnumerable<LinkedInPersonBase> persons)
        {
            _RelatedProfileViews.AddRange(persons);
        }

        internal void AddMemberUrls(IEnumerable<LinkedInMemberUrl> urls)
        {
            _MemberUrls.AddRange(urls);
        }

        internal void AddRecommendations(IEnumerable<LinkedInRecommendation> recommendations)
        {
            _RecommendationsReceived.AddRange(recommendations);
        }

        internal void AddJobBookmarks(IEnumerable<LinkedInJobBookmark> jbookmarks)
        {
            _JobBookmarks.AddRange(jbookmarks);
        }

        internal void AddConnections(IEnumerable<LinkedInPersonBase> connections)
        {
            _Connections.AddRange(connections);
        }

        #endregion
    }
}
